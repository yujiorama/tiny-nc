tiny-nc(netcat)
====

[nc(1)](https://linux.die.net/man/1/nc)

arbitrary TCP and UDP connections and listens

## Status

* `[-i interval]` []
* `[-p source_port]` []
* `[-s source_ip_address]` []
* `[-T ToS]` []
* `[-w timeout]` [/]
* `[-X proxy_protocol]` []
* `[-x proxy_address[:port]]` []
* `[hostname]` [/]
* `[port]` [/]

## install

```bash
go get -u bitbucket.org/yujiorama/tiny-nc
```

```bash
git clone https://bitbucket.org/yujiorama/tiny-nc
cd tiny-nc
go install
```

## License

MIT
