package main

import (
	"flag"
	"fmt"
	"net"
	"os"
	"time"
)

func usage() {
	commandLine := flag.NewFlagSet(os.Args[0], flag.ExitOnError)
	fmt.Fprintf(commandLine.Output(), "%s (-timeout timeout second) [host] [port]\n", os.Args[0])
	flag.PrintDefaults()
	fmt.Fprintf(commandLine.Output(), "  host\n")
	fmt.Fprintf(commandLine.Output(), "        hostname or ip address\n")
	fmt.Fprintf(commandLine.Output(), "  port\n")
	fmt.Fprintf(commandLine.Output(), "        port number\n")
}

func main() {
	timeout := flag.Duration("timeout", time.Duration(3)*time.Second, "timeout second")
	flag.Parse()

	if len(flag.Args()) < 2 {
		usage()
		os.Exit(1)
	}
	address := fmt.Sprintf("%s:%s", flag.Arg(0), flag.Arg(1))
	_, err := net.DialTimeout("tcp", address, *timeout)
	if err != nil {
		os.Exit(1)
	}
	os.Exit(0)
}
